variable "site" {
  description = "subdomain (if no dots) or FQDN of the site"
}

variable "zone" {
  description = "DNS zone in which to create the record for the website"
  default = ""
}

variable "proxied" {
  description = "Flag (bool) to specify if DNS record should use CloudFlare proxy and SSL"
  default = false
}
