locals {
  zone = "${var.zone != "" ? var.zone : var.site}"
  # if `var.site` contains a '.', use it as domain (FQDN), otherwise
  # use `${var.site}.${var.zone}`
  domain = "${contains(split("", var.site), ".") ? var.site : join(".", list(var.site, var.zone))}"
}

data "aws_region" "current" {}

module "staticsite" {
  source  = "justincampbell/staticsite/aws"
  version = "0.2.1"

  bucket = "${local.domain}"
  domain = "${local.domain}"
  region = "${data.aws_region.current.name}"
}

resource "cloudflare_record" "dns" {
  domain = "${local.zone}"
  name = "${var.site}"
  value = "${module.staticsite.website_endpoint}"
  type = "CNAME"
  proxied = "${var.proxied}"
}
