output "dns_zone" {
  description = "DNS zone in which the domain record is created"
  value = "${local.zone}"
}

output "website_fqdn" {
  description = "DNS domain of the website"
  value = "${cloudflare_record.dns.hostname}"
}

output "website_endpoint" {
  description = "S3 website endpoint"
  value = "${module.staticsite.website_endpoint}"
}

output "dns_record_name" {
  value = "${cloudflare_record.dns.name}"
}

output "dns_record_type" {
  value = "${cloudflare_record.dns.type}"
}

output "dns_record_ttl" {
  value = "${cloudflare_record.dns.ttl}"
}

output "dns_record_priority" {
  value = "${cloudflare_record.dns.priority}"
}

output "dns_record_proxied" {
  value = "${cloudflare_record.dns.proxied}"
}
